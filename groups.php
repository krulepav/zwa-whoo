<?php include("functions.php"); homeIfNotSession(); ?>
<!DOCTYPE html>
<html>
  <head>    
    <meta charset="UTF-8">
    <title>
      Whoo - Groups
    </title>              
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel=icon href="favicon.png" sizes="any" type="image/png">                                                  
    <link rel="stylesheet" type="text/css" href="w3.css">    
    <link rel="stylesheet" href="w3-theme-blue.css">
    <link rel="stylesheet" type="text/css" href="style.css" />
    
    <script>
      function init()
      {
      document.getElementById("new-group-btn").addEventListener("click", function(){document.getElementById("new-group-popup").style.display="block";}, false);
      document.getElementById("ng-close-btn").addEventListener("click", function(){document.getElementById("new-group-popup").style.display="none";}, false);
      document.getElementById("join-group-btn").addEventListener("click", function(){document.getElementById("join-group-popup").style.display="block";}, false);
      document.getElementById("jg-close-btn").addEventListener("click", function(){document.getElementById("join-group-popup").style.display="none";}, false);
      }    
      
      function removeGroup(id) 
      {
        event.stopPropagation();
        if(confirm("Are you sure to remove this group from your list?\nYou won't be removed from any item in this group as long as there's someone else in this group after you leave. If you are the last person in this group, then the whole group with all of its items will be completely and permanently removed."))
        {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              document.getElementById("groupsContainer").innerHTML = xhttp.responseText;
            }
          }
          xhttp.open("POST", "remove-gu.php", true);
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send("groupId="+id);
        }
      }                                                                                                                       
    </script>
  </head>
  <body class="w3-content" onload="init()"> 
    <noscript><div class=" w3-container w3-red">It looks like you have JavaScript disabled. This website needs JavaScript to work properly.</div></noscript>
    <div class="w3-card-2">
      <header class="w3-container w3-theme">
        <?php include("part-logo.php"); ?>
        <h2 class="w3-animate-right">Groups</h2> 
      </header>  
      <nav class="w3-topnav w3-theme-dark">
        <a href="groups.php">Goups</a>
        <a href="account.php">Account</a>
        <a href="logout.php" class="w3-right">Sign out</a>
      </nav>
    </div>
      <section class="w3-container">
        <h3>Select group</h3>
        <input type="button" id="join-group-btn" class="w3-btn w3-theme w3-margin-2" value="Join existing group">
        <input type="button" id="new-group-btn" class="w3-btn w3-theme w3-margin-2" value="Create new group">


        <div id="groupsContainer">
        <?php
        
        $conn = connectDB();
        generateGroupsLi($conn, $_SESSION["user"]->id);
        $conn->close();
        ?>
        </div>
      </section>  
      
      <!-- MODALS BEGIN -->
      <div id="new-group-popup" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="ng-close-btn" class="w3-closebtn">&times;</span>
            <h2>Create new group</h2>
          </header>
          <div class="w3-container">
            <form action="create-group.php" method="post" class="w3-form">
              <div class="common-form">
              <input type="text" name="group-name" placeholder="Group name" required class="w3-input"><br />
              <input type="text" name="group-code" placeholder="Joining code" required class="w3-input"><br /> 
              <input type="submit" value="Create" class="w3-btn w3-theme w3-right">
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo create new group</p>
          </footer>
        </div>
      </div>     
      <!-- MODAL NEXT --> 
      <div id="join-group-popup" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="jg-close-btn" class="w3-closebtn">&times;</span>
            <h2>Join existing group</h2>
          </header>
          <div class="w3-container">
            <form action="join-group.php" method="post" class="w3-form">
              <div class="common-form">
              <input type="text" name="group-name" placeholder="Group name" required class="w3-input"><br />
              <input type="text" name="group-code" placeholder="Joining code" required class="w3-input"><br /> 
              <input type="submit" value="Join" class="w3-btn w3-theme w3-right">
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo join existing  group</p>
          </footer>
        </div>
      </div>  
      <!-- MODALS END -->
           
      <footer class="w3-container w3-theme-light w3-card-2">
        Whoo © 2015-2016 Pavel Krulec
      </footer>
  </body>
</html>