<?php
include("functions.php"); 
if(!isset($_POST["group-name"])){echo "<script>history.back();</script>"; return;} 

session_start();

$name = $code = "";

$name = validateInput($_POST["group-name"]); 
$code = validateInput($_POST["group-code"]);
  
$conn = connectDB();
 
if(!joinGroup($conn, $name, $code))
{
  $conn->close();
  alertError('ERROR: Wrong name/code or you already are a member');
}
else
{     
  $conn->close();
  header("Location: groups.php");  
}
?>