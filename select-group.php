<?php
include("functions.php");  
if(!isset($_GET["groupId"])){echo "<script>history.back();</script>"; return;}
     
session_start();

$groupId = $userId = "";

$groupId = validateInput($_GET["groupId"]); 
$userId = $_SESSION["user"]->id;//validateInput($_POST["group-code"]);
  
$conn = connectDB();
$result = $conn->query("SELECT id FROM gu WHERE groupId = $groupId AND userId = $userId"); 
if($result->num_rows == 0)
{
  $conn->close();
  alertError('ERROR: You are not a member of this groups');
}
else
{     
  $row = $conn->query("SELECT * FROM groups WHERE id = $groupId")->fetch_assoc();                  
  $group = new StdClass();
  $group->id = $row["id"];
  $group->name = $row["name"];
  $group->code = $row["code"];
  $group->users = getUsersInGroup($conn, $groupId);
  $_SESSION["group"] = $group;
  $conn->close();
  header("Location: group-dashboard.php");  
}
?>