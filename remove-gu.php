<?php
include("functions.php");
if(!isset($_POST["groupId"])){echo "<script>history.back();</script>"; return;} 
session_start();

$groupId = $userId = "";

$groupId = validateInput($_POST["groupId"]);
$userId = $_SESSION["user"]->id;
  
$conn = connectDB();
 
if(!removeGU($conn, $groupId, $userId))
{
  $conn->close();
  alertError("ERROR: Group wasn't removed");
}
else
{                      
  generateGroupsLi($conn, $userId);
  $conn->close(); 
}
?>