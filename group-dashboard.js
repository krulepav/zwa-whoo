
function animateChart()
{
  for (i = 0; i < ids.length; i++)
  {    
    if(document.getElementById(ids[i][0]) != null)
    {   
      document.getElementById(ids[i][0]).firstElementChild.innerHTML = Math.abs(ids[i][1]);
      document.getElementById(ids[i][0]).style.width = (Math.abs(ids[i][1]/maxValue)*100+"%");
    }
  }   
}

function init()
{
  document.getElementById("new-item-btn-big").addEventListener("click", function(){document.getElementById("new-item-popup").style.display="block";}, false);
  document.getElementById("new-item-btn-small").addEventListener("click", function(){document.getElementById("new-item-popup").style.display="block";}, false);
  document.getElementById("payback-btn").addEventListener("click", function(){document.getElementById("payback-popup").style.display="block";}, false);
  
  document.getElementById("item-close-btn").addEventListener("click", function(){document.getElementById("new-item-popup").style.display="none";}, false);
  document.getElementById("payback-close-btn").addEventListener("click", function(){document.getElementById("payback-popup").style.display="none";}, false);
  
  //items paging is done using AJAX
  document.getElementById("page").addEventListener("change", function()
  {
    event.stopPropagation();
    var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          document.getElementById("itemTable").innerHTML = xhttp.responseText;
        }
      }
      xhttp.open("POST", "generate-items-table.php", true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send("itemPage="+document.getElementById("page").value);
  }, false);    
  
  document.getElementById("datePicker").valueAsDate = new Date();
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();
  if(mm<10){
    mm="0"+mm
  } 
  if(dd<10){
    dd="0"+dd
  } 
  document.getElementById("datePicker").max = yyyy+"-"+mm+"-"+dd;
  
  document.getElementById("chartHeadLeft").innerHTML = maxValue;
  document.getElementById("chartHeadRight").innerHTML = maxValue;
  
  
  document.getElementById("joining-code-btn").addEventListener("click", 
  function()
  {   
     var code = document.getElementById("joining-code-text");   
     code.style.width = (code.style.width == "0px" || code.style.width == "") ? "150px" : "0px";
     code.select();
  }, false);
  
  document.getElementById("joining-code-text").addEventListener("click", 
  function()
  {
     this.select();
  }, false);
}    

function removeItem(id) 
{
  event.stopPropagation();
  if(confirm("Are you sure to remove this item?"))
  {
    window.location = "remove-ui.php?itemId="+id;
  }
} 