<?php                
include("functions.php");  
session_start();
$conn = connectDB();
$items = getItemsInGroup($conn, $_SESSION["group"]->id, $_POST["itemPage"]);
echo generateItemsTable($conn, $items);
$conn->close();
?>