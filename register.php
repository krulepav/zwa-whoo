<?php
include("functions.php");     
      if(!isset($_POST["email"])){echo "<script>history.back();</script>"; return;}
      
      if(validateInput($_POST["password"]) != validateInput($_POST["password-re"]))
      {
        alertError('ERROR: Passwords are not the same');
        return;
      }
      $firstName = $lastName = $email = $password = "";
      
      $firstName = validateInput($_POST["firstName"]); 
      $lastName = validateInput($_POST["lastName"]);
      $email = validateInput($_POST["email"]);
      $password = password_hash(validateInput($_POST["password"]), PASSWORD_DEFAULT);
        
      $conn = connectDB();
       
      if(!registerUser($conn, $firstName, $lastName, $email, $password))
      {
        $conn->close();
        alertError('ERROR: E-mail already exists');
      }
      else
      {     
        session_start();
        $_SESSION["user"] = login($conn, $email, $_POST["password"]);
        $conn->close();
        
          $to = $email;
          $subject = "Whoo registration confirmation";
          $message = '
          <div style="max-width:980px;margin:auto;">
          <div style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;" >
                <header style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;color:#fff !important;background-color:#2196F3 !important;" >
                  <h1 style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;font-family:\'Segoe UI\',Arial,sans-serif;font-weight:400;margin-top:10px;margin-bottom:10px;margin-right:0;margin-left:0;font-size:64px !important;" ><a href="https://www.whoo-beta.tk" style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-tap-highlight-color:transparent;background-color:transparent;color:inherit;font-weight:inherit;text-decoration:none;" ><img src="https://www.whoo-beta.tk/whoo-logo.png" alt="Whoo logo" style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border-width:0;margin-bottom:-5px;margin-right:16px !important;margin-left:8px;box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;width:64px;height:64px;" />Whoo<sub style="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;line-height:0;position:relative;vertical-align:baseline;bottom:-0.25em;font-size:.25em;" >&beta;</sub></a></h1> 
            </header>
          </div>
          <p>
          Dear '.$firstName.',<br />
          thank you for your registration. Please confirm this e-mail address <a href="https://whoo-beta.tk/confirm-email.php?email='.urlencode($email).'&code='.substr(md5($email."whoo.salt"), 5, 5).'">here</a> or go to <a href="https://whoo-beta.tk/confirm-email.php">https://whoo-beta.tk/confirm-email.php</a> and enter following details manually:<br />
          <ul>
            <li>E-mail: <b>'.$email.'</b></li>
            <li>Code: <b>'.substr(md5($email."whoo.salt"), 5, 5).'</b></li>
          </ul>
          Enjoy our service :)
          </p>
        </div>
         ';
         
          $headers   = array();
          $headers[] = "MIME-Version: 1.0";
          $headers[] = "Content-type: text/html; charset=UTF-8";
          $headers[] = "From: whoo-beta.tk <no-reply@whoo-beta.tk>";
          //$headers[] = "Bcc: JJ Chong <bcc@domain2.com>";
          //$headers[] = "Reply-To: Recipient Name <receiver@domain3.com>";
          //$headers[] = "Subject: {$subject}";
          //$headers[] = "X-Mailer: PHP/".phpversion();
          //echo $email;
          mail($to, $subject, $message, implode("\r\n", $headers));
        
        header("Location: confirm-email.php?email=".urlencode($email));  
      }
?>