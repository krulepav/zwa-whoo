<?php
session_start();
if(isset($_COOKIE["id"]) && isset($_COOKIE["hash"]))
{   
  include("functions.php");
  $conn = connectDB();
    
  $result = $conn->query("SELECT * FROM users WHERE id = '".$_COOKIE["id"]."'");
  if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    if($_COOKIE["hash"] == $row["rememberHash"])
    {
      $user = new StdClass();
      $user->id = $row["id"];
      $user->firstName = $row["firstName"];
      $user->lastName = $row["lastName"];
      $user->email = $row["email"];
      $user->validated = $row["validated"]; 
      $user->passwordHash = $row["password"]; 
      $_SESSION["user"] = $user;  
    }
    else
    {
      //return false;
    }
  }
  else
  {           
    //return false;
  }       
  $conn->close();
}
if(isset($_SESSION["user"]))
{
  header("Location: groups.php");  
}                     
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>
      Whoo beta
    </title>              
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel=icon href="favicon.png" sizes="any" type="image/png">                                                
    <link rel="stylesheet" type="text/css" href="w3.css">  
    <link rel="stylesheet" href="w3-theme-blue.css">
    <link rel="stylesheet" type="text/css" href="style.css" />
    
    <script>
      function init()
      {
      document.getElementById("forgot-pwd-text").addEventListener("click", function(){document.getElementById("forgot-password").style.display="block";}, false);
      document.getElementById("close-btn").addEventListener("click", function(){document.getElementById("forgot-password").style.display="none";}, false);
      }                  
      
      function validateRegistration()
      {        
        var error = "";
        var form = document.forms["registration"];
        if (form["firstName"].value == null || form["firstName"].value.length == 0) {
            error += "First name must be filled out\n";
        }
        if (form["lastName"].value == null || form["lastName"].value.length == 0) {
            error += "Last name must be filled out\n";
        }
        if (form["email"].value == null || form["email"].value.length == 0) {
            error += "E-mail must be filled out\n";
        }
        if (form["password"].value == null || form["password"].value.length == 0) {
            error += "Password must be filled out\n";
        }
        if (form["password-re"].value != form["password"].value) {
            error += "Passwords are not the same\n";
        }
        if (!form["agree"].checked) {
            error += "You must agree with conditions";
        }
        
        
        if(error != "")
        {
          alert(error);
          return false;
        }
        
      }                                                                                                                
    </script>
    
  </head>
  <body class="w3-content" onload="init()">    
    <noscript><div class=" w3-container w3-red">It looks like you have JavaScript disabled. This website needs JavaScript to work properly.</div></noscript>
      <header class="w3-container w3-theme w3-card-2">
      <?php include("part-logo.php"); ?>
      <h2 class="w3-animate-right">An expenses and debts tracking system</h2>
      </header>
      <div class="w3-row">
      <section class="w3-half w3-container">
        <div class="common-form">
        <h3>Sign in:</h3>
        <form action="login.php" method="post" class="w3-form">          
            <input type="email"    name="email"    placeholder="E-mail"   required class="w3-input"> <br />
            <!-- RegEx source: http://regexlib.com/REDetails.aspx?regexp_id=31 -->
            <input type="password" name="password" placeholder="Password" required class="w3-input" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$"> <br />
            <!--<label><input type="checkbox" name="remember">Remember </label><input type="submit" value="Sign in"><br />-->
            <label class="w3-checkbox">
              <input type="checkbox" name="remember" value="remember">
              <span class="w3-checkmark"></span> Remember
            </label>
            <input type="submit" value="Sign in" class="w3-btn w3-theme w3-right">
            <br />
            <!--<a href="./forgot-password.html">Forgot password?</a>-->
            <a href="javascript:;" id="forgot-pwd-text">Forgot password?</a>
            
        </form>
        </div>
      </section>
      <section class="w3-half w3-container">
        <div class="common-form"> 
        <h3>Sign up:</h3>
        <form action="register.php" method="post" class="w3-form" onsubmit="return validateRegistration()" name="registration">
            <input type="text"     name="firstName"   placeholder="First name"                   required class="w3-input"> <br />
            <input type="text"     name="lastName"    placeholder="Last name"                    required class="w3-input"> <br />
            <input type="email"    name="email"       placeholder="E-mail"                       required class="w3-input"> <br />   
            <!-- RegEx source: http://regexlib.com/REDetails.aspx?regexp_id=31 -->
            <input type="password" name="password"    placeholder="Password (min. 6, a-Z + 0-9)" required class="w3-input" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$"> <br />
            <input type="password" name="password-re" placeholder="Password again"               required class="w3-input" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$"> <br />
            <!--<label><input type="checkbox" name="agree">I agree with conditions</label><br />-->
            <label class="w3-checkbox">
              <input type="checkbox" name="agree">
              <span class="w3-checkmark"></span> I agree with conditions
            </label>
            
            <input type="submit" value="Sign up" class="w3-btn w3-theme w3-right">
        </form>
        </div>
      </section>
      
      <!-- MODALS BEGIN -->
      <div id="forgot-password" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="close-btn" class="w3-closebtn">&times;</span>
            <h2>Password reset</h2>
          </header>
          <div class="w3-container">
            <p>
            So you've forgotten your password, huh? What a shame! Well, we don't store your password, therefore it is not possible
            to send you your password in any case. But wait, we've got good news for you! We can generate a new password and send
            it to your e-mail address. It is highly recommended to change the generated password as soon as you sign in using it!
            </p>
            <form action="/" method="post">
              <div class="common-form">
              <input type="email" name="email" placeholder="Your e-mail" required class="w3-input"><br /> 
              <input type="submit" value="Reset password" class="w3-btn w3-theme w3-right">
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo password reset</p>
          </footer>
        </div>
      </div>
     <!-- MODALS END -->
      
      </div>
      <footer class="w3-container w3-theme-light w3-card-2">
        Whoo © 2015-2016 Pavel Krulec
      </footer>
  </body>
</html>