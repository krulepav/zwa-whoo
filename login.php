<?php
include("functions.php");       
      if(!isset($_POST["email"])){echo "<script>history.back();</script>"; return;} 

      $email = $password = "";
      
      $email = validateInput($_POST["email"]);
      $password = validateInput($_POST["password"]);
        
      $conn = connectDB();
       
      if(!($user = login($conn, $email, $password)))
      {
        $conn->close();
        alertError('ERROR: Wrong e-mail or password');
      }
      else
      {     
        session_start();
        $_SESSION["user"] = $user;
        //echo "Yes! Welcome ".$user->firstName." ".$user->lastName." ";
        if(isset($_POST["remember"]) && $user->validated == 1)
        {
          rememberUserSession($conn, $user);
          //echo "remember";
        }
        else
        {
          //logout();
          //echo "not";
        }
        
        $conn->close();
        
        if($user->validated){
          header("Location: groups.php"); 
        }
        else {
          header("Location: confirm-email.php?email=".urlencode($email));  
        }
      }
?>