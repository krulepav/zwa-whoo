<?php
include("functions.php");        
      if(!isset($_POST["email"])){echo "<script>history.back();</script>"; return;}

      $email = $code = "";
      
      $email = validateInput($_POST["email"]);
      $code = validateInput($_POST["code"]);
        
      $conn = connectDB();
       
      if(!validateUser($conn, $email, $code))
      {
        $conn->close();
        alertError("ERROR: Wrong e-mail $email or code $code");
      }
      else
      {     
        $conn->close();
        //echo "Validated";
        session_start();
        if(isset($_SESSION["user"]))
        {
          $_SESSION["user"]->validated = 1;
        }
        header("Location: groups.php"); 
      }
?>