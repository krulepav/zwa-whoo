<?php
      include("functions.php");     

      if(!isset($_POST["payback"])){echo "<script>history.back();</script>"; return;} 
      session_start();
            
      $conn = connectDB();
      
      foreach($_POST["payback"] as $id=>$payback)
      {
        //echo $id." - ".$payback."<br>";
        if($payback <= 0) {continue;}        
        if(!addPayback($conn, userFromId($conn, $id), $payback))
        {
          $conn->close();
          alertError("ERROR: Payback wasn't created");
        }
      }
      $conn->close();
      header("Location: group-dashboard.php");  


 


?>
