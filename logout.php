<?php
include("functions.php");

session_start();

if(isset($_SESSION["user"]))
{
  $conn = connectDB();
  logout($conn, $_SESSION["user"]);
  $conn->close();
  //echo "Logged out";    
  header("Location: index.php"); 
}
else                                              
{       
  //alertError('ERROR: Not logged out');   
  header("Location: index.php"); 
}
?>