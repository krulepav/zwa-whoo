<?php include("functions.php"); homeIfNotSession(); 
$user = $_SESSION["user"];
?>
<!DOCTYPE html>
<html>
  <head>      
    <meta charset="UTF-8">
    <title>
      Whoo - Account
    </title>              
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link rel=icon href="favicon.png" sizes="any" type="image/png">                                               
    <link rel="stylesheet" type="text/css" href="w3.css">       
    <link rel="stylesheet" href="w3-theme-blue.css">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="chart-style.css" />
    <script>
      function init()
      {
        document.getElementById("chpwd-btn").addEventListener("click", function(){document.getElementById("chpwd-popup").style.display="block";}, false);
        document.getElementById("chpwd-close-btn").addEventListener("click", function(){document.getElementById("chpwd-popup").style.display="none";}, false);
      }
    </script>
    
  </head>
  <body class="w3-content" onload="init();">
    <noscript><div class=" w3-container w3-red">It looks like you have JavaScript disabled. This website needs JavaScript to work propertly.</div></noscript> 
    <div class="w3-card-2">
      <header class="w3-container w3-theme">
        <?php include("part-logo.php"); ?>  
        <h2 class="w3-animate-right">Account</h2>
      </header>
      <nav class="w3-topnav w3-theme-dark">
        <a href="groups.php">Goups</a>
        <a href="account.php">Account</a>
        <a href="logout.php" class="w3-right">Sign out</a>
      </nav>
    </div>   
      <div class="w3-container">
        <section>
        <h3>Account overview</h3>
          <table class="w3-bordered w3-border hoverable w3-table"> 
                      
            <tr>
              <td>
                First name
              </td>
              <td>
                <?php echo $user->firstName; ?>
              </td> 
            </tr>            
            <tr>
              <td>
                Last name
              </td>
              <td>
                <?php echo $user->lastName; ?>
              </td> 
            </tr>            
            <tr>
              <td>
                E-mail name
              </td>
              <td>
                <?php echo $user->email; ?>
              </td> 
            </tr>
           
          </table>
          <input type="button" class="w3-btn w3-theme w3-margin-top" value="Change password" id="chpwd-btn">
        </section>
        
       <!-- MODALS BEGIN -->
       <div id="chpwd-popup" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="chpwd-close-btn" class="w3-closebtn">&times;</span>
            <h2>Change password</h2>
          </header>
          <div class="w3-container">
            <form action="change-password.php" method="post" class="w3-form">
              <div class="common-form">
              <input type="password" name="oldPassword" placeholder="Old password" required class="w3-input"><br />
              <input type="password" name="newPassword" placeholder="New password (min. 6, a-Z + 0-9)" required class="w3-input" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$"><br />
              <input type="password" name="reNewPassword" placeholder="New password again" required class="w3-input" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$"><br /> 
              <input type="submit" value="Change" class="w3-btn w3-theme w3-right">
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo change password</p>
          </footer>
        </div>
      </div> 
      <!-- MODALS END -->
        
      </div>  
      <footer class="w3-container w3-theme-light w3-card-2">
        Whoo © 2015-2016 Pavel Krulec
      </footer>
  </body>
</html>