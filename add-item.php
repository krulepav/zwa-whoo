<?php
include("functions.php");  
if(!isset($_POST["itemPrice"])){echo "<script>history.back();</script>"; return;} 
session_start();

$itemDate = $itemDesc = $itemPrice = $itemRecievers = "";

$itemDate = validateInput($_POST["itemDate"]); 
$itemDesc = validateInput($_POST["itemDesc"]);
$itemPrice = validateInput($_POST["itemPrice"]); 
$itemRecievers = $_POST["itemRecievers"];
  
$conn = connectDB();
 
if(!addItem($conn, $itemDate, $itemDesc, $itemPrice, $itemRecievers))
{
  $conn->close();
  alertError("ERROR: Item wasn't created");
}
else
{     
  $conn->close();
  header("Location: group-dashboard.php");  
}
?>