<?php


/**
 *Connects to DB and returns reference to this connection
 *@return connection
 **/
function connectDB() {
  $DBservername = "localhost";
  $DBusername = "zwa";
  $DBpassword = "sit.FEL";
  $DBdbname = "whoo";
        
  $conn = new mysqli($DBservername, $DBusername, $DBpassword, $DBdbname);
  
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
      } 
  $conn->query("SET NAMES 'utf8'");
  return $conn;
}

/**
 *Register user with provided credentials
 *@param mysqli $conn Connection to the database
 *@param string firstName User's first name
 *@param string lastName User's last name
 *@param string email User's e-mail
 *@param string password User's password
 *@return bool True if registered, otherwise False
 **/
function registerUser($conn, $firstName, $lastName, $email, $password){
  if ($conn->query("SELECT email FROM users WHERE email = '$email'")->num_rows > 0) {
    return false;  
  }
  $sql = "INSERT INTO users (firstName, lastName, email, password)
        VALUES ('$firstName', '$lastName', '$email', '$password')";
            
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
  return true;
}

/**
 *Log user in and return object with user info
 *@param mysqli $conn Connection to the database
 *@param string $email User's e-mail
 *@param string $password User's password
 *@return User User object if logged, otherwise false
 **/
function login($conn, $email, $password){
  $result = $conn->query("SELECT * FROM users WHERE email = '$email'");
  if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    if(password_verify($password, $row["password"]))
    {
      $user = new StdClass();
      $user->id = $row["id"];
      $user->firstName = $row["firstName"];
      $user->lastName = $row["lastName"];
      $user->email = $row["email"];
      $user->validated = $row["validated"];    
      $user->passwordHash = $row["password"]; 
      return $user;
    }
    else
    {
      return false;
    }
  }
  else
  {           
    return false;
  }                
}

/**
 *Prevent user's input to harm this web
 *@param string $data String to be escaped
 *@return string Escaped string
 **/
function validateInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data, ENT_QUOTES);
  return $data;
}

/**
 *Validate user's e-mail with a code
 *@param mysqli $conn Connection to the database
 *@param string $email E-mail to be validated
 *@param string $code Verification code
 *@return bool True if validated, otherwise False
 **/
function validateUser($conn, $email, $code) {
 $result = $conn->query("SELECT * FROM users WHERE email = '$email'");
  if ($result->num_rows > 0 && $code == substr(md5($email."whoo.salt"), 5, 5)) {                            
    if($conn->query("UPDATE users SET validated = true WHERE email = '$email'") === true)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {           
    return false;
  }                  
}

/**
 *If user checked "Remember me", save cookie for autologin
 *@param mysqli $conn Connection to the database
 *@param User User object to remember
 *@return bool True if successfull
 **/
function rememberUserSession($conn, $user)
{
  $hash = md5($user->email.time());
  setcookie("id", $user->id, (time()+(60*60*24*30)), "/");
  setcookie("hash", $hash, (time()+(60*60*24*30)), "/");    //Just a random hash                    
  if($conn->query("UPDATE users SET rememberHash = '$hash' WHERE id = '".$user->id."'") === true)
  {
    return true;
  }
  else
  {
    return false;
  }    
}

/**
 *Prevent user to enter any other page except index if not logged in
 *or force him to validate his e-mail if logged but not validated  yet
 **/
function homeIfNotSession()
{
  session_start();
  if(!isset($_SESSION["user"]))
  {
    header("Location: index.php"); 
  }
  else if($_SESSION["user"]->validated == 0)
  {
    header("Location: confirm-email.php?email=".urlencode($_SESSION["user"]->email));  
  }
}

/**
 *Log user out and destroy cookies if user was auto logged in
 *@param mysqli $conn Connection to the database
 *@param User $user User to log out
 *@return bool True if successful
 **/
function logout($conn, $user)
{
  session_unset(); 
  session_destroy();
  
  $conn->query("UPDATE users SET rememberHash = NULL WHERE id = '$user->id'");
    
  if(isset($_COOKIE["id"]))
  {
    setcookie("id", "", mktime()-3600);
    setcookie("hash", "", mktime()-3600);
  }
}

/**
 *Create new group with a joining code
 *@param mysqli $conn Connection to the database
 *@param string $name Name of the group
 *@param string $code Joining code of the group
 *@return bool True if successful, otherwise False
 **/
function createGroup($conn, $name, $code)
{
  //Don't create group if its name is already taken
  if ($conn->query("SELECT id FROM groups WHERE name = '$name'")->num_rows > 0) {
    return false;  
  }
  $sql = "INSERT INTO groups (name, code)
        VALUES ('$name', '$code')";
            
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    return false;
  }
  
  //After creating a group automaticly join current user
  $groupId = $conn->query("SELECT id FROM groups WHERE name = '$name'")->fetch_assoc()["id"];
  $sql = "INSERT INTO gu (groupId, userId)
        VALUES ('$groupId', '".$_SESSION["user"]->id."')";
            
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
  return true;
}

/**
 *Join user to an existing group
 *@param mysqli $conn Connection to the database
 *@param string $name Name of the group
 *@param string $code Joining code
 *@return bool True if successful, otherwise False
 **/
function joinGroup($conn, $name, $code)
{
  //If group name of the joining code doesn't match, stop
  $sql = $conn->query("SELECT id FROM groups WHERE name = '$name' AND code = '$code'");
  if ($sql->num_rows == 0) { 
    return false;  
  }
  $groupId = $sql->fetch_assoc()["id"];

  //If logged user is already a member of this group, stop
  $sql = $conn->query("SELECT id FROM gu WHERE groupId = '".$groupId."' AND userId = '".$_SESSION["user"]->id."'");
  if ($sql->num_rows > 0) {   
    return false;  
  }
  
  //Join current user to the group
  $sql = "INSERT INTO gu (groupId, userId) VALUES ('".$groupId."', '".$_SESSION["user"]->id."')";          
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: 3<br>" . $conn->error;
    return false;
  }
  return true;
}

/**
 *Generate &lt;li&gt;'s with groups which the logged user is member of
 *@param mysqli $conn Connection to the database
 *@param int $userId Id of the currently logged user
 *@return string HTML code with the list
 **/
function generateGroupsLi($conn, $userId)
{
      $sql = "SELECT groups.id, name, code FROM groups JOIN gu ON gu.groupId = groups.id AND gu.userId = $userId ORDER BY gu.id DESC";
        $result = $conn->query($sql);
        
        if ($result && $result->num_rows > 0) {
            echo '<p>
                  Or select one of the groups you are already member of:
                  </p> 
                  <ul class="w3-ul hoverable w3-border">';
            while($row = $result->fetch_assoc()) {
                echo "<li onclick=\"location.href='select-group.php?groupId=".$row["id"]."'\" style=\"cursor:pointer;\"> <noscript><a href=\" select-group.php?groupId=".$row["id"]." \"></noscript>" . $row["name"]. "<noscript></a></noscript> <span class=\"w3-right\" onclick=\"removeGroup('".$row["id"]."')\">&times;</span></li>";
            }                                                                                                        
            echo "</ul>";
        } else {
            echo "<p>You are not in any group yet.</p>";
        }
}

/**
 *Remove user from group
 *@param mysqli $conn Connection to the database
 *@param int $groupId ID of the group to remove
 *@param int $useriD ID of the user for whom remove the group
 *@return bool True if successful, otherwise False
 **/
function removeGU($conn, $groupId, $userId)
{
  $sql = "DELETE FROM gu WHERE groupId = $groupId AND userId = $userId";
  $result = $conn->query($sql);
  
  if (!$result) 
  {
       return false;
  }
  else
  {
    $sql = "SELECT id FROM gu WHERE groupId = $groupId";
    $result = $conn->query($sql);
    if($result->num_rows == 0)      //If there's not other user in this group
    {                               //delete the whole group
      $sql = "DELETE FROM groups WHERE id = $groupId";
      $result = $conn->query($sql);
    }
    return true;
  }
}

/**
 *Returns all the users in a group with all their info
 *@param mysqli $conn Connection to the database
 *@param int $groupId The id of the group to get users of
 *@return User[] Array of Users in selected group
 **/
function getUsersInGroup($conn, $groupId)
{
   $result = $conn->query("SELECT users.* FROM users JOIN gu ON users.id = gu.userId AND gu.groupId = $groupId");
   $users = [];
   $i = 0;
   while($row = $result->fetch_assoc())
   {
      $user = new StdClass();
      $user->id = $row["id"];
      $user->firstName = $row["firstName"];
      $user->lastName = $row["lastName"];
      $user->email = $row["email"];
      $user->validated = $row["validated"];
      $users[$i] = $user;
      $i++;
   }
   return $users;
}

/**
 *Add a new item to a group
 *@param mysqli $conn Connection to the database
 *@param string $itemDate Date of buying the item
 *@param string $itemPrice Price of the item
 *@param User[] $itemRecievers Array of users who should pay the item
 *@return bool True if successful, otherwise Faůse
 **/
function addItem($conn, $itemDate, $itemDesc, $itemPrice, $itemRecievers)
{
  $sql = "INSERT INTO items (date, groupId, ownerId, description, price)
        VALUES ('$itemDate', '".$_SESSION["group"]->id."', '".$_SESSION["user"]->id."', '$itemDesc', '$itemPrice')";
            
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
  $itemId = $conn->insert_id;
  
  //For each reciever of this item make a record in DB
  for($i = 0; $i < count($itemRecievers); $i++)
  {
    $sql = "INSERT INTO ui (userId, itemId) VALUES ('".$itemRecievers[$i]."', '$itemId')";
              
    if ($conn->query($sql) === TRUE) {
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
  return true;
}

/**
 *Add a payback to a group
 *@param mysqli $conn Connection to the database
 *@param User $payer User, who paid back
 *@param int $amount Amount of money paid back
 *@return bool True if successful
 **/
function addPayback($conn, $payer, $amount)
{
  $itemDate = date("Y-m-d");
  $itemDesc = "PB from ".$payer->firstName."&nbsp;".$payer->lastName;
  $itemPrice = $amount;
  $itemRecievers = $payer;

  $sql = "INSERT INTO items (date, groupId, ownerId, description, price, isPayback)
        VALUES ('$itemDate', '".$_SESSION["group"]->id."', '".$_SESSION["user"]->id."', '$itemDesc', '$itemPrice', '1')";
            
  if ($conn->query($sql) === TRUE) {
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
  $itemId = $conn->insert_id;
  //For each reciever of this item make a record in DB
  for($i = 0; $i < count($itemRecievers); $i++)
  {
    $sql = "INSERT INTO ui (userId, itemId) VALUES ('".$itemRecievers->id."', '$itemId')";
              
    if ($conn->query($sql) === TRUE) {
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
  return true;
}

/**
 *Return $itemsPerPage items created in a group.
 *For $page holds: 1 <= $page <= [numberOfItems]/$itemsPerPage
 *@param mysqli $conn Connection to the database
 *@param int $groupId ID of group
 *@param int $page Used for paging
 *@param int $pagesCount If set, returns total number of pages
 *@return Item[] All items in this group if $page is null, or as set
 **/
function getItemsInGroup($conn, $groupId, $page = null, &$pagesCount = null)
{
   $itemsPerPage = 10;
   if(!is_null($page) && !is_null($pagesCount))
   {
      $pagesCount = ceil($conn->query("SELECT * FROM items WHERE groupId = $groupId")->num_rows / $itemsPerPage);
   }
   if(!is_null($page))
   {
      //$result = $conn->query("SELECT * FROM items WHERE (isPayback = 0 OR (isPayback = 1 AND (ownerId = ".$_SESSION["user"]->id."))) AND groupId = $groupId ORDER BY id DESC LIMIT $itemsPerPage OFFSET ".$itemsPerPage*($page-1));
      $result = $conn->query("SELECT * FROM items WHERE (isPayback = 0 OR (isPayback = 1 AND (ownerId = ".$_SESSION["user"]->id."))) AND groupId = $groupId ORDER BY id DESC LIMIT $itemsPerPage OFFSET ".$itemsPerPage*($page-1));
   }
   else
   {
      $result = $conn->query("SELECT * FROM items WHERE groupId = $groupId ORDER BY id DESC");
   }
   $items = [];
   $i = 0;
   while($row = $result->fetch_assoc())
   {
      $item = new StdClass();
      $item->id = $row["id"];
      $item->date = $row["date"];
      $item->groupId = $row["groupId"];
      $item->ownerId = $row["ownerId"];
      $item->desc = $row["description"];
      $item->price = $row["price"];
      $item->isPayback = $row["isPayback"];
      
      $rec = [];  //recievers
      
      //Add recivers to an item
      $useres = $conn->query("SELECT users.id FROM users JOIN ui ON users.id = ui.userId AND ui.itemId = ".$item->id);// groupId = $groupId ORDER BY id DESC");
      $j = 0;
      while($userow = $useres->fetch_assoc())
      {
        $rec[$j] = userFromId($conn, $userow["id"]);
        $j++;
      }
      $item->rec = $rec;
      $items[$i] = $item;
      $i++;
   }
   return $items;
}

/**
 *Get user object from id
 *@param mysqli $conn Connection to the database
 *@param string $userId ID of user to convert to User object
 *@return User User object with corresponding ID
 **/
function userFromId($conn, $userId)
{
  $result = $conn->query("SELECT * FROM users WHERE id = '$userId'");
  if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $user = new StdClass();
    $user->id = $row["id"];
    $user->firstName = $row["firstName"];
    $user->lastName = $row["lastName"];
    $user->email = $row["email"];
    $user->validated = $row["validated"];
    return $user;
  }
  else
  {
    return false;
  }  
}

/**
 *Delete item and all records associated with it
 *@param mysqli $conn Connection to the database
 *@param int $itemId ID of item to remove
 *@return bool True if successful, otherwise False
 **/
function removeUI($conn, $itemId)
{
  $sql = "DELETE ui, items FROM ui, items WHERE ui.itemId = $itemId AND items.id = $itemId";
  $result = $conn->query($sql);
  
  if (!$result) 
  {
      echo $conn->error;
       return false;
  }
  else
  {
    return true;
  }
}

/**
 *For each users generate a row with his name in debts chart
 *where JS later draws depts bars
 *@param User[] $users Array of Users for which the rows should be generated
 *@return string HTML code
 **/
function generateChartRows($users)
{        
  for($i = 0; $i < sizeof($_SESSION["group"]->users); $i++)
  {
  $user = $users[$i];
  if($user->id == $_SESSION["user"]->id || $user->owes == 0) {continue;}
  echo '
  <tr class="w3-border">
    <td class="w3-border-right">
      '.$user->firstName."&nbsp;".$user->lastName.'
    </td>
    <td>';
    if($user->owes < 0)
      {
      echo '
      <div id="id'.$user->id.'" class="chart-bar-div-left">            
        <div class="chart-bar"></div>
      </div>';
      }
      echo '</td>  <td>';
    if($user->owes > 0)
      {
      echo '
      <div id="id'.$user->id.'" class="chart-bar-div-right">            
        <div class="chart-bar"></div>
      </div>';
      }
    echo '</td>
  </tr>';
  }
}

/**
 *From the view of logged user compute how much
 *do the others owe him or how much does
 *the logged user owe to the others
 *@param mysqli $conn Connection to the database
 *@param User[] &$users Array of Users to calculate debts for
 *@param Item[] &$items Array of Items to calculate debts from
 **/
function setUsersDebts($conn, &$users, &$items)
{
  //Set debt 0 for each user
  for($j = 0; $j < count($users); $j++)
    {
       $users[$j]->owes = 0;//rand(-1000, 1000);
    }
    
  //Go through all of the items, find which owns current user
  //and increase debts to all recivers of those items  
  //or decrease if item is a payback
  for($i = 0; $i < count($items); $i++)
  {
    if($items[$i]->ownerId == $_SESSION["user"]->id && $items[$i]->groupId == $_SESSION["group"]->id)
    {
        $result = $conn->query("SELECT * FROM ui WHERE itemId = ".$items[$i]->id);
        while($row = $result->fetch_assoc())
        {
           $user = null;
           for($j = 0; $j < count($users); $j++) 
           {
              if($row["userId"] == $users[$j]->id){$user = $users[$j]; break;}
           }
           //echo (isset($owner) && isset($items[$i]));
           //if(isset($owner) && isset($items[$i]))
           if(!$items[$i]->isPayback)
           {$user->owes+=round($items[$i]->price/count($items[$i]->rec));} //, 2);}
           else
           {$user->owes-=round($items[$i]->price/count($items[$i]->rec));}
        }
    }
  }   
  
  
  //Decrease debts for items which were bought for current user by someone else 
  //(where the logged user is a reciver)
  $itemResult = $conn->query("SELECT itemId FROM ui WHERE userId = ".$_SESSION["user"]->id);
  while($itemRow = $itemResult->fetch_assoc())
  {
      $result = $conn->query("SELECT * FROM items WHERE id = ".$itemRow["itemId"]);
      
      
      while($row = $result->fetch_assoc())
      {
          $owner = null;
          for($j = 0; $j < count($users); $j++) 
          {
             if($row["ownerId"] == $users[$j]->id){$owner = $users[$j]; break;}
          }
          $item = null;
          for($j = 0; $j < count($items); $j++) 
          {
             if($row["id"] == $items[$j]->id){$item = $items[$j]; break;}
          }           
          /*if(isset($owner) && isset($item))
          {$owner->owes -= round($item->price/count($item->rec));}//, 2);}*/
          if(isset($owner) && isset($item))
          {
              if(!$row["isPayback"])
                {$owner->owes -= round($item->price/count($item->rec));}
              else
                {$owner->owes += round($item->price/count($item->rec));}
          } 
      }       
  }     
}

/**
 *Show error popup and go to the previous page
 *@param string $error Message to show
 **/
function alertError($error)
{
  echo "<script>                                  
  history.back();
  alert('$error');
  </script>";
  echo "$error"."<br>Please press the back button in your web browser.";
  return;
}

/**
 *Generate table with items and their info
 *@param mysqli $conn Connection to the database
 *@param Item[] $items Array of items to generate table for
 *@return string HTML code
 **/
function generateItemsTable($conn, $items)
{
$text = "";
$text .= '<table class="w3-table w3-bordered w3-border hoverable">
            <tr>
              <th>  
                Item
              </th>
              <th> 
                Owner
              </th> 
              <th class="w3-hide-small">    
                Date
              </th>    
              <th class="w3-hide-small">
                Recievers
              </th>
              <th class="w3-hide-small">     
                Price total
              </th>
              <th>
                Price / prsn 
              </th>
            </tr>
            ';
 for($i = 0; $i<count($items); $i++)
          { 
            $itemUser = userFromId($conn, $items[$i]->ownerId);
            $itemRecievers = $items[$i]->rec;
            $date = (new DateTime($items[$i]->date))->format("d.m.Y");
            $isPayback = $items[$i]->isPayback;
            
            $text .= "<tr ";
            if($isPayback)
              {
                $text .= "class='payback-row'";
                $text .= ">
                <td>       
                  "."PB from ".$itemRecievers[0]->firstName."&nbsp;".$itemRecievers[0]->lastName."
                </td>
                <td>          
                  ".$itemUser->firstName."&nbsp;".$itemUser->lastName."
                </td> 
                <td class=\"w3-hide-small\">   
                  ".$date."
                </td>  
                <td class=\"w3-hide-small\">
                  ";
            $text .=  $itemRecievers[0]->firstName."&nbsp;".$itemRecievers[0]->lastName." -> ".$itemUser->firstName."&nbsp;".$itemUser->lastName;  
            $text .=  "
                </td>
                <td class=\"w3-hide-small\">
                  ".$items[$i]->price."
                </td>
                <td>
                  ".round($items[$i]->price/count($items[$i]->rec), 2);
              }
            else //if not payback
            {  
            $text .= ">
                <td>       
                  ".$items[$i]->desc."
                </td>
                <td>          
                  ".$itemUser->firstName."&nbsp;".$itemUser->lastName."
                </td> 
                <td class=\"w3-hide-small\">   
                  ".$date."
                </td>  
                <td class=\"w3-hide-small\">
                  ";
            $text .=  $itemRecievers[0]->firstName."&nbsp;".$itemRecievers[0]->lastName;
            for($j = 1; $j < count($itemRecievers); $j++)
            {
              $text .=  ", ".$itemRecievers[$j]->firstName."&nbsp;".$itemRecievers[$j]->lastName;
            }    
            $text .=  "
                </td>
                <td class=\"w3-hide-small\">
                  ".$items[$i]->price."
                </td>
                <td>
                  ".round($items[$i]->price/count($items[$i]->rec), 2);
            }       
                  
            if($_SESSION["user"]->id == $items[$i]->ownerId) {$text .=  "<span class=\"w3-right\" onclick=\"removeItem('".$items[$i]->id."')\" style=\"cursor:pointer;\">&times;</span>";}     
            $text .=  "
                </td>
              </tr>";
            }
$text .= '</table>';
return $text;
}
?>
