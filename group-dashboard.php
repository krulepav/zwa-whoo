<?php include("functions.php"); homeIfNotSession(); if(!isset($_SESSION["group"])){header("Location: groups.php"); }?>
<?php 
$conn = connectDB();

$_SESSION["group"]->users = getUsersInGroup($conn, $_SESSION["group"]->id);

$pages = 0;
$items = getItemsInGroup($conn, $_SESSION["group"]->id);
//$items = getItemsInGroup($conn, $_SESSION["group"]->id, 1, $pages);
$users = getUsersInGroup($conn, $_SESSION["group"]->id); 
$maxDebt = 0;
setUsersDebts($conn, $users, $items);
$items = getItemsInGroup($conn, $_SESSION["group"]->id, 1, $pages); //After setting debts re-load items to be "paged"

for($j = 0; $j < count($users); $j++)
{
  if(abs($users[$j]->owes)>$maxDebt && $users[$j]->id != $_SESSION["user"]->id){$maxDebt = abs($users[$j]->owes);}
}
$maxDebt = ceil($maxDebt/(pow(10, strlen(round($maxDebt))-1))) * (pow(10, strlen(round($maxDebt))-1));
$conn->close();


$g = $_SESSION["group"];
?>
<!DOCTYPE html>
<html>
  <head>      
    <meta charset="UTF-8">
    <title>
      Whoo - Group dashboard
    </title>              
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <link rel=icon href="favicon.png" sizes="any" type="image/png">                                            
    <link rel="stylesheet" type="text/css" href="w3.css">       
    <link rel="stylesheet" href="w3-theme-blue.css">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="chart-style.css" />
    <script>
      //var ids = [["id0", 500], ["id1", 250], ["id2", 750]];

      var ids = [
      <?php
      for($j = 0; $j < count($users); $j++)
      {
        if($_SESSION["user"]->id != $users[$j]->id)
         {
         echo "['id".$users[$j]->id."', ".$users[$j]->owes."], "; 
         }
      } 
      ?>];  
      var maxValue = <?php echo $maxDebt; ?>;
    </script>
    <script src="group-dashboard.js"></script>
  </head>
  <body class="w3-content" onload="init(); animateChart();">
    <noscript><div class=" w3-container w3-red">It looks like you have JavaScript disabled. This website needs JavaScript to work properly.</div></noscript> 
    <div class="w3-card-2">
      <header class="w3-container w3-theme">
        <?php include("part-logo.php");?>
        <h2 class="w3-animate-right">Group dashboard</h2>
      </header>
      <nav class="w3-topnav w3-theme-dark">
        <a href="groups.php">Goups</a>
        <a href="account.php">Account</a>
        <a href="logout.php" class="w3-right">Sign out</a>
      </nav>
    </div>   
      <div class="w3-container">
        <h3><?php echo $_SESSION["group"]->name; ?></h3>
        <p>
        <?php 
        if(count($g->users) > 1)
        {
          echo "There are ". count($g->users)." members in group ". $g->name." in total: ";
        }
        else
        {
          echo "There is ". count($g->users)." member in group ". $g->name." in total: ";
        }
        echo $g->users[0]->firstName."&nbsp;".$g->users[0]->lastName;
        for($i = 1; $i < count($g->users); $i++)
        {
          echo ", ".$g->users[$i]->firstName."&nbsp;".$g->users[$i]->lastName."";
        }
        ?>
        </p>
        <p>
          <input type="button" class="w3-btn w3-theme-light" value="Joining code" id="joining-code-btn">
          <input type="text" value="<?php echo $_SESSION["group"]->code;?>" id="joining-code-text" readonly class="w3-theme-light w3-large">
        </p>
        <section>
        <h3>Ballance graph</h3>
        Let's take a look at your ballance: <br /> 
        <div id="graph-container">
            <table class="w3-border">
               <tr class="w3-border">
                <th style="width: 30%;" class="w3-border-right">
                  Name
                </th>
                <th style="width: 35%;" class="w3-border-right">
                   You owe
                </th>
                <th style="width: 35%;">
                   Owes you 
                </th>
              </tr>
            
              <?php
              generateChartRows($users);
              ?>
              
              <tr class="w3-border">
                <td class="w3-border-right">
                
                </td>
                <td>
                   <span class="w3-left" id="chartHeadLeft"></span>
                </td>
                <td>
                   <span class="w3-right" id="chartHeadRight"></span> 
                </td>
              </tr>
            </table>
        </div>
        </section>
        <hr />
        <section>
          <h3>Last items</h3> 
          <p class="w3-bottom">     
          <span id="new-item-btn-small" class="w3-btn-floating-large w3-theme-dark w3-hide-large w3-hide-medium w3-margin-24 w3-right">+</span>
          </p>
          <p>
            <input type="button" id="new-item-btn-big" class="w3-btn w3-theme-dark w3-hide-small" value="Add new item">
            Page:  
            <select id="page">                  
                <?php 
                for($i = 0; $i < $pages; $i++)
                {
                  echo "<option value='".($i+1)."'>".($i+1)."</option>";
                }
                ?>                  
            </select>
            <?php echo " of $pages"; 
            
            $paybackDisabled = true;
            for($i = 0; $i < sizeof($_SESSION["group"]->users); $i++)
            {
                $user = $users[$i];
                if($user->id == $_SESSION["user"]->id || $user->owes <= 0) {continue;}
                else {$paybackDisabled = false; break;}
            }
            if($paybackDisabled)
            {
              echo '<input type="button" id="payback-btn" class="w3-btn w3-right" value="Paid me back" disabled>';
            }
            else
            {
              echo '<input type="button" id="payback-btn" class="w3-btn w3-theme w3-right" value="Paid me back">';
            }
            ?>
          </p>
          <div id="itemTable">            
          <?php 
          $conn = connectDB();
          //$items = getItemsInGroup($conn, $_SESSION["group"]->id);
          echo generateItemsTable($conn, $items);
          $conn->close();
          ?>
          </div>
          <p class="w3-hide-large w3-hide-medium"><em>Note:</em> Some less important columns were hidden to fit the table to your screen. View this site on a screen wider than 992px to see all of the columns.</p>
        </section>     
      
      <!-- MODALS BEGIN -->
      <div id="new-item-popup" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="item-close-btn" class="w3-closebtn">&times;</span>
            <h2>Add new item</h2>
          </header>
          <div class="w3-container">
            <form action="add-item.php" method="post" class="w3-form">
              <div class="common-form">
              <input type="date" name="itemDate" min="1996-04-07" class="w3-input" id="datePicker" required><br />
              <input type="text" name="itemDesc" placeholder="Description" required class="w3-input"><br />
              <input type="number" name="itemPrice" placeholder="Total price" required class="w3-input" min="0"><br /> 
              <select name="itemRecievers[]" class="w3-select" multiple  required >
                <option value="" disabled>Recievers (who should pay)</option>
                <option value="" disabled class="w3-hide-small">Select multiple using Ctrl</option>
                <?php 
                for($i = 0; $i < count($g->users); $i++)
                {
                  echo "<option value='".$g->users[$i]->id."'>".$g->users[$i]->firstName." ".$g->users[$i]->lastName."</option>";
                }
                ?>
              </select> <br /><br /> 
              <input type="submit" value="Add item" class="w3-btn w3-theme w3-right">
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo add new item</p>
          </footer>
        </div>
      </div> 
      <!-- MODAL NEXT -->
      <div id="payback-popup" class="w3-modal">
        <div class="w3-modal-content w3-card-4">
          <header class="w3-container w3-theme"> 
            <span id="payback-close-btn" class="w3-closebtn">&times;</span>
            <h2>Paid me back</h2>
          </header>
          <div class="w3-container">
            <form action="payback.php" method="post" class="w3-form">
              <div class="common-form">
               <?php
                for($i = 0; $i < sizeof($_SESSION["group"]->users); $i++)
                {
                $user = $users[$i];
                if($user->id == $_SESSION["user"]->id || $user->owes <= 0) {continue;}
                echo '
                  <p>
                  <label><b>'.$user->firstName."&nbsp;".$user->lastName.'</b><br />
                    <input type="number" placeholder="...paid me" class="w3-input" style="display: inline; max-width: 300px;" min="0" max="'.$user->owes.'" id="pb-tb-usr'.$user->id.'" name="payback['.$user->id.']">
                  </label><input type="button" value="'.$user->owes.'" class="w3-btn w3-theme w3-right" style="display: inline; width: 75px;" onclick="getElementById(\'pb-tb-usr'.$user->id.'\').value = '.$user->owes.'"><br />
                  </p>
                ';
                }
              ?>
              <input type="submit" value="Submit" class="w3-btn w3-theme">                            
              </div>
            </form>
          </div>
          <footer class="w3-container w3-theme">
            <p>Whoo paid me back</p>
          </footer>
        </div>
      </div> 
      <!-- MODALS END -->
      
      </div>
      <footer class="w3-container w3-theme-light w3-card-2">
        Whoo © 2015-2016 Pavel Krulec
      </footer>
  </body>
</html>