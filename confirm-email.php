<!DOCTYPE html>
<html>
  <head>    
    <meta charset="UTF-8">
    <title>
      Whoo - Confirm e-mail
    </title>              
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <link rel=icon href="favicon.png" sizes="any" type="image/png">                                           
    <link rel="stylesheet" type="text/css" href="w3.css"> 
    <link rel="stylesheet" href="w3-theme-blue.css">
    <link rel="stylesheet" type="text/css" href="style.css" />
  </head>
  <body class="w3-content">  
  
      <header class="w3-container w3-theme w3-card-2">
        <?php include("part-logo.php"); ?>
        <h2 class="w3-animate-right">Confirm your e-mail</h2>      
      </header> 
      <nav class="w3-topnav w3-theme-dark">
        <a href="logout.php" class="w3-right">Sign out</a>
      </nav>
      <div class="w3-row">
      <div class="w3-container w3-half">
        <p>
        Your e-mail address hasn't been confirmed yet. Please check your e-mail inbox, there should be a message with
        a code and a link. You can confirm your e-mail either by clicking the link or by writing down the code.
        </p>
        <p>
        If there is no e-mail in your inbox:
        </p>
        <ol>
          <li>Check spam</li>
          <li>Wait 30 minutes</li>
          <li>Request resending e-mail</li>  
          <li>Wait 24 hours</li>
          <li>Contact support</li> 
          <li>Wait 7 days</li>    
          <li>Give it up</li>
        </ol>
      </div>
      <section class="w3-container w3-half">        
        <div class="common-form">
        <h3>Confirm e-mail</h3>
        <form action="validate-email.php" method="post" class="w3-form">
            <?php //if(isset($_GET["email"])) { echo "Since e-mail confirmation does not work currently, your code is: ".substr(md5($_GET["email"]."whoo.salt"), 5, 5); } ?>
            <input type="email" name="email" placeholder="E-mail"           class="w3-input" <?php if(isset($_GET["email"])) {echo 'value="'.$_GET["email"].'" readonly';} else {echo "required";}?> > <br />
            <input type="text"  name="code"  placeholder="Code from e-mail" class="w3-input" <?php if(isset($_GET["code"])) {echo 'value="'.$_GET["code"].'"';} ?> required> <br />
            <input type="button" value="Resend e-mail" class="w3-btn w3-theme-light"><input type="submit" value="Confirm e-mail" class="w3-btn w3-theme w3-right">
        </form>
        </div>
      </section> 
      </div> 
      <footer class="w3-container w3-theme-light w3-card-2">
        Whoo © 2015-2016 Pavel Krulec
      </footer>
  </body>
</html>