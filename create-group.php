<?php
include("functions.php");   
if(!isset($_POST["group-name"])){echo "<script>history.back();</script>"; return;} 
session_start();

$name = $code = "";

$name = validateInput($_POST["group-name"]); 
$code = validateInput($_POST["group-code"]);
  
$conn = connectDB();
 
if(!createGroup($conn, $name, $code))
{
  $conn->close();
  alertError("ERROR: Group $name already exists");
}
else
{     
  $conn->close();
  header("Location: groups.php");  
}
?>