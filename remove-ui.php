<?php
include("functions.php");
if(!isset($_GET["itemId"])){echo "<script>history.back();</script>"; return;} 
                      
session_start();

$itemId = "";

$itemId = validateInput($_GET["itemId"]);
  
$conn = connectDB();
 
if($conn->query("SELECT * FROM items WHERE id = $itemId AND ownerId = ".$_SESSION["user"]->id)->num_rows == 0)
{
  $conn->close();
  alertError("ERROR: You don't own this item");
}
else
{
  if(!removeUI($conn, $itemId))
  {
    $conn->close();
    alertError("ERROR: Item wasn't removed");
  }
  else
  {                      
    $conn->close();
    header("Location: group-dashboard.php");  
  }
}
?>