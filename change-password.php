<?php
include("functions.php");
      if(!isset($_POST["newPassword"])){echo "<script>history.back();</script>"; return;} 
      
      if(validateInput($_POST["newPassword"]) != validateInput($_POST["reNewPassword"]))
      {
        alertError('ERROR: Passwords are not the same');
        return;
      }
      session_start();    
      
      if(!password_verify(validateInput($_POST["oldPassword"]), $_SESSION["user"]->passwordHash))
      {
        alertError('ERROR: Wrong old password');
        return;
      }
      
      $password = password_hash(validateInput($_POST["newPassword"]), PASSWORD_DEFAULT);
        
      $conn = connectDB();  
              
      if($conn->query("UPDATE users SET password = '$password' WHERE id = ".$_SESSION["user"]->id))
      {
        $conn->close();      
        $_SESSION["user"]->passwordHash = $password;                   
        header("Location: account.php"); 
      }
      else
      {     
        $conn->close();   
        alertError('ERROR: Password not changed');
      }
?>